package co.com.ceiba.mobile.pruebadeingreso.view.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.user.User;
import co.com.ceiba.mobile.pruebadeingreso.view.OnUserInteraction;

public class UserAdapter extends RecyclerView.Adapter {

    private List<User> users;
    private OnUserInteraction onUserInteraction;

    public UserAdapter(List<User> users, OnUserInteraction onUserInteraction){

        this.users = users;
        this.onUserInteraction = onUserInteraction;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);

        UserHolder userHolder = new UserHolder(v);

        return userHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        UserHolder userHolder = (UserHolder) holder;

        userHolder.name.setText(users.get(position).getName());

        userHolder.phone.setText(users.get(position).getPhone());

        userHolder.email.setText(users.get(position).getEmail());

        userHolder.viewPosts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                onUserInteraction.onGoToUserPosts(users.get(position).getId());

            }

        });
    }

    @Override
    public int getItemCount() {

        return users.size();

    }

    private class UserHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView phone;
        TextView email;
        Button viewPosts;

        UserHolder(View itemView) {

            super(itemView);

            name = itemView.findViewById(R.id.name);

            phone = itemView.findViewById(R.id.phone);

            email = itemView.findViewById(R.id.email);

            viewPosts = itemView.findViewById(R.id.btn_view_post);

        }

    }

}