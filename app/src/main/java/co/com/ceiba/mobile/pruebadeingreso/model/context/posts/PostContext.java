package co.com.ceiba.mobile.pruebadeingreso.model.context.posts;

import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.model.context.AppError;
import co.com.ceiba.mobile.pruebadeingreso.model.context.posts.local.PostLocal;
import co.com.ceiba.mobile.pruebadeingreso.model.context.posts.remote.PostsRemote;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.post.Post;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;

public class PostContext {

    private Realm realm;
    private PostLocal local;
    private CompositeDisposable compositeDisposable;

    public PostContext() {

        realm = Realm.getDefaultInstance();

        compositeDisposable = new CompositeDisposable();

        local = new PostLocal(realm, compositeDisposable);

    }

    public void getUserPosts(final PostsCompletion postsCompletion, final int userId) {

        local.getAllUserPosts(new PostsCompletion() {

            @Override
            public void onGetUserPosts(List<Post> userPost) {

                if(userPost.isEmpty()) {

                    PostsRemote.getAllUserPosts(postsCompletion, userId);

                }else {

                    postsCompletion.onGetUserPosts(userPost);

                }

            }

            @Override
            public void onInsertPost(AppError appError) {

            }

        }, userId);

    }

    public void insertPosts(final List<Post> users) {

        local.insertPost(users.get(0), new PostsCompletion() {

            int index = 0;

            @Override
            public void onGetUserPosts(List<Post> posts) {

            }

            @Override
            public void onInsertPost(AppError appError) {

                if(++ index < users.size()) {

                    local.insertPost(users.get(index), this);

                }

            }

        });

    }

}
