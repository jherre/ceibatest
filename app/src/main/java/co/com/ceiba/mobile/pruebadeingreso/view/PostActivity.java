package co.com.ceiba.mobile.pruebadeingreso.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.model.context.AppError;
import co.com.ceiba.mobile.pruebadeingreso.model.context.posts.PostContext;
import co.com.ceiba.mobile.pruebadeingreso.model.context.posts.PostsCompletion;
import co.com.ceiba.mobile.pruebadeingreso.model.context.user.UserCompletion;
import co.com.ceiba.mobile.pruebadeingreso.model.context.user.UserContext;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.user.User;
import co.com.ceiba.mobile.pruebadeingreso.view.adapters.PostAdapter;

public class PostActivity extends Activity {

    private TextView name;
    private TextView phone;
    private TextView email;
    private UserContext userContext;
    private PostContext postContext;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_post);

        name = findViewById(R.id.name);

        phone = findViewById(R.id.phone);

        email = findViewById(R.id.email);

        LinearLayoutManager llm = new LinearLayoutManager(this);

        recyclerView = findViewById(R.id.recyclerViewPostsResults);

        recyclerView.setLayoutManager(llm);


        int userId = getIntent().getIntExtra("userId", 0);

        userContext = new UserContext();

        postContext = new PostContext();

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                "Loading. Please wait...", true);

        userContext.onGetUser(new UserCompletion() {

            @Override
            public void onGetUsers(List<User> users) {

                if(! users.isEmpty()) {

                    User user = users.get(0);

                    name.setText(user.getName());

                    phone.setText(user.getPhone());

                    email.setText(user.getEmail());

                }

            }

            @Override
            public void onInsertUser(AppError appError) {

            }

        }, userId);

        postContext.getUserPosts(new PostsCompletion() {

            @Override
            public void onGetUserPosts(List<Post> posts) {

                if(posts != null) {

                    recyclerView.setAdapter(new PostAdapter(posts));

                    if (!posts.isEmpty()) {

                        postContext.insertPosts(posts);

                    }

                }

                dialog.dismiss();

            }

            @Override
            public void onInsertPost(AppError appError) {

            }

        }, userId);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }


}
