package co.com.ceiba.mobile.pruebadeingreso.model.domain.user;

public class Company {

    private String name;
    private String catchPhrase;
    private String bs;

    public String getName() {

        return name;

    }

    public String getBs() {

        return bs;

    }

    public String getCatchPhrase() {

        return catchPhrase;

    }

    @Override
    public String toString() {

        return "Company{" +
                "name='" + name + '\'' +
                ", catchPhrase='" + catchPhrase + '\'' +
                ", bs='" + bs + '\'' +
                '}';

    }

}
