package co.com.ceiba.mobile.pruebadeingreso.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.model.context.AppError;
import co.com.ceiba.mobile.pruebadeingreso.model.context.user.UserCompletion;
import co.com.ceiba.mobile.pruebadeingreso.model.context.user.UserContext;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.user.User;
import co.com.ceiba.mobile.pruebadeingreso.view.adapters.UserAdapter;

public class MainActivity extends Activity implements OnUserInteraction{

    private RecyclerView recyclerView;
    private UserContext userContext;
    private EditText searchUserEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        LinearLayoutManager llm = new LinearLayoutManager(this);

        recyclerView = findViewById(R.id.recyclerViewSearchResults);

        searchUserEditText = findViewById(R.id.editTextSearch);

        recyclerView.setLayoutManager(llm);

        userContext = new UserContext();

        getAllUsers();

        searchUserEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(! TextUtils.isEmpty(searchUserEditText.getText())) {

                    userContext.onSearchUsers(new UserCompletion() {

                        @Override
                        public void onGetUsers(List<User> users) {

                            if(users != null) {

                                recyclerView.setAdapter(new UserAdapter(users, MainActivity.this));

                                if(users.isEmpty()) {

                                    final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, "",
                                                "List is empty", true);

                                    dialog.setCancelable(true);

                                }

                            }

                        }

                        @Override
                        public void onInsertUser(AppError appError) {

                        }

                    }, searchUserEditText.getText().toString());

                }else {

                    getAllUsers();

                }

            }

        });

    }

    public void getAllUsers() {

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                "Loading. Please wait...", true);

        userContext.getAllUsers(new UserCompletion() {

            @Override
            public void onGetUsers(List<User> users) {

                if(users != null) {

                    recyclerView.setAdapter(new UserAdapter(users, MainActivity.this));

                    userContext.insertUsers(users);

                }

                dialog.dismiss();

            }

            @Override
            public void onInsertUser(AppError appError) {

            }

        });

    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    public void onGoToUserPosts(int userId) {

        Intent intent = new Intent(this, PostActivity.class);

        intent.putExtra("userId", userId);

        startActivity(intent);

    }

}