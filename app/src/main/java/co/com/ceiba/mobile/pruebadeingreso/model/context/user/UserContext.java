package co.com.ceiba.mobile.pruebadeingreso.model.context.user;

import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.model.context.AppError;
import co.com.ceiba.mobile.pruebadeingreso.model.context.user.local.UserLocal;
import co.com.ceiba.mobile.pruebadeingreso.model.context.user.remote.UserRemote;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.user.User;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;

public class UserContext {

    private Realm realm;
    private UserLocal local;
    private CompositeDisposable compositeDisposable;
    private UserRemote remote;

    public UserContext() {

        realm = Realm.getDefaultInstance();

        compositeDisposable = new CompositeDisposable();

        local = new UserLocal(realm, compositeDisposable);

        remote = new UserRemote();

    }

    public void getAllUsers(final UserCompletion userCompletion) {

       local.getAllUsers(new UserCompletion() {

           @Override
           public void onGetUsers(List<User> users) {

               if(users.isEmpty()) {

                   remote.getAllUsers(userCompletion);

               }else {

                   userCompletion.onGetUsers(users);

               }

           }

           @Override
           public void onInsertUser(AppError appError) {

           }
       });

    }

    public void insertUsers(final List<User> users) {

        local.insertUser(users.get(0), new UserCompletion() {

            int index = 0;

            @Override
            public void onGetUsers(List<User> users) {

            }

            @Override
            public void onInsertUser(AppError appError) {

                if(++ index < users.size()) {

                    local.insertUser(users.get(index), this);

                }

            }

        });

    }

    public void onSearchUsers(UserCompletion userCompletion, String value){

        local.getSearchResultsCustomers(userCompletion, value);

    }

    public void onGetUser(UserCompletion userCompletion, int value) {

        local.getUser(userCompletion, value);

    }
}