package co.com.ceiba.mobile.pruebadeingreso.model.context.user;

import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.model.context.AppError;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.user.User;

public interface UserCompletion {

    void onGetUsers(List<User> users);

    void onInsertUser(AppError appError);

}
