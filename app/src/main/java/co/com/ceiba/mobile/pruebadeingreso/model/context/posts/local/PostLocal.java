package co.com.ceiba.mobile.pruebadeingreso.model.context.posts.local;

import android.util.Log;
import java.util.ArrayList;
import co.com.ceiba.mobile.pruebadeingreso.model.context.AppError;
import co.com.ceiba.mobile.pruebadeingreso.model.context.posts.PostsCompletion;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.post.Post;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subscribers.DisposableSubscriber;
import io.realm.Realm;
import io.realm.RealmResults;

public class PostLocal {

    private Realm realm;
    private CompositeDisposable compositeDisposable;

    public PostLocal(Realm realm, CompositeDisposable compositeDisposable) {

        this.realm = realm;

        this.compositeDisposable = compositeDisposable;

    }

    public void getAllUserPosts(final PostsCompletion userCompletion, int userId) {

        Flowable<RealmResults<Post>> customers = realm.where(Post.class).equalTo("userId", userId).findAllAsync().asFlowable().observeOn(AndroidSchedulers.mainThread());

        DisposableSubscriber disposableSubscriber = new DisposableSubscriber<RealmResults<Post>>() {

            @Override
            public void onNext(RealmResults<Post> posts) {

                userCompletion.onGetUserPosts(posts);

            }

            @Override
            public void onError(Throwable t) {

                userCompletion.onGetUserPosts(new ArrayList<Post>());

            }

            @Override

            public void onComplete() {
                Log.e("@@","completed" );
            }

        };

        customers.subscribeWith(disposableSubscriber);

        compositeDisposable.add(disposableSubscriber);

    }


    public void insertPost( final Post post, final PostsCompletion customerCompletion) {

        realm.executeTransactionAsync(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {

                realm.insert(post);

            }

        }, new Realm.Transaction.OnSuccess() {

            @Override
            public void onSuccess() {

                customerCompletion.onInsertPost(null);

            }

        }, new Realm.Transaction.OnError() {

            @Override
            public void onError(Throwable error) {

                AppError appError;

                if (error.getMessage().contains("Value already exists:")) {

                    appError = new AppError(getClass().toString() + post.toString(), AppError.valueAlreadyExists);

                } else {

                    appError = new AppError(getClass().toString() + post.toString(), error.getMessage().hashCode());

                }

                customerCompletion.onInsertPost(appError);

            }

        });

    }

    }
