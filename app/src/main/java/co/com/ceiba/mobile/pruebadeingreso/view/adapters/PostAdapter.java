package co.com.ceiba.mobile.pruebadeingreso.view.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.post.Post;

public class PostAdapter extends RecyclerView.Adapter {

    private List<Post> posts;

    public PostAdapter(List<Post> posts) {

        this.posts = posts;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_item, parent, false);

        PostAdapter.PostHolder postHolder = new PostAdapter.PostHolder(v);

        return postHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        PostHolder postHolder = (PostHolder) holder;

        postHolder.title.setText(posts.get(position).getTitle());

        postHolder.body.setText(posts.get(position).getBody());

    }

    @Override
    public int getItemCount() {

        return posts.size();

    }

    private class PostHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView body;

        PostHolder(View itemView) {

            super(itemView);

            title = itemView.findViewById(R.id.title);

            body = itemView.findViewById(R.id.body);

        }

    }
}
