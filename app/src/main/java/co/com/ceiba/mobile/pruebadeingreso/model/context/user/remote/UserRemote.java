package co.com.ceiba.mobile.pruebadeingreso.model.context.user.remote;

import android.util.Log;

import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.model.context.user.UserCompletion;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.user.User;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserRemote {

    public static void getAllUsers(final UserCompletion userCompletion) {

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Endpoints.URL_BASE).addConverterFactory(GsonConverterFactory.create()).build();

        UserRemoteService userRemoteService = retrofit.create(UserRemoteService.class);

        Call<List<User>> listUsers = userRemoteService.listUsers();

        listUsers.enqueue(new Callback<List<User>>() {

            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                if(response.isSuccessful()) {

                    List<User> users = response.body();

                    userCompletion.onGetUsers(users);

                    for(User user : users) {

                        Log.e("user", user.toString());

                    }

                }else {

                    userCompletion.onGetUsers(null);

                }

            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

                userCompletion.onGetUsers(null);

            }

        });

    }

}
