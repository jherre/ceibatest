package co.com.ceiba.mobile.pruebadeingreso.base;

import android.app.Application;
import android.graphics.Typeface;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BaseApplication extends Application {


    @Override
    public void onCreate() {

        super.onCreate();

        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);

    }


}
