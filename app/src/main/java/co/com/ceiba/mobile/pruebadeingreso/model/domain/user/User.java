package co.com.ceiba.mobile.pruebadeingreso.model.domain.user;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {

    @PrimaryKey
    private int id;
    private String name;
    private String username;
    private String email;
    @Ignore
    private Address address;
    private String phone;
    private String website;
    @Ignore
    private Company company;

    public Address getAddress() {

        return address;

    }

    public int getId() {

        return id;

    }

    public String getEmail() {

        return email;

    }

    public String getName() {

        return name;

    }

    public String getUsername() {

        return username;

    }

    public String getPhone() {

        return phone;

    }

    public Company getCompany() {

        return company;

    }

    public String getWebsite() {

        return website;

    }

    @Override
    public String toString() {

        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                ", phone='" + phone + '\'' +
                ", website='" + website + '\'' +
                ", company=" + company +
                '}';

    }

}
