package co.com.ceiba.mobile.pruebadeingreso.view;

public interface OnUserInteraction {

    void onGoToUserPosts(int userId);

}
