package co.com.ceiba.mobile.pruebadeingreso.model.domain.user;

public class Address {

    private String street;
    private String suite;
    private String city;
    private String zipcode;
    private Geo geo;

    public String getCity() {

        return city;

    }

    public Geo getGeo() {

        return geo;

    }

    public String getStreet() {

        return street;

    }

    public String getSuite() {

        return suite;

    }

    public String getZipcode() {

        return zipcode;

    }

    @Override
    public String toString() {

        return "Address{" +
                "street='" + street + '\'' +
                ", suite='" + suite + '\'' +
                ", city='" + city + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", geo=" + geo +
                '}';

    }

}