package co.com.ceiba.mobile.pruebadeingreso.model.context.posts.remote;

import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PostsRemoteService {

    @GET(Endpoints.GET_POST_USER)
    Call<List<Post>> listPost(@Query("userId") int userId);

}
