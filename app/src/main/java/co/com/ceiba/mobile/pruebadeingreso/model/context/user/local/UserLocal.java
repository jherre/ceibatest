package co.com.ceiba.mobile.pruebadeingreso.model.context.user.local;

import android.util.Log;
import java.util.ArrayList;
import co.com.ceiba.mobile.pruebadeingreso.model.context.AppError;
import co.com.ceiba.mobile.pruebadeingreso.model.context.user.UserCompletion;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.user.User;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subscribers.DisposableSubscriber;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

public class UserLocal {

    private Realm realm;
    private CompositeDisposable compositeDisposable;

    public UserLocal(Realm realm, CompositeDisposable compositeDisposable) {

        this.realm = realm;

        this.compositeDisposable = compositeDisposable;

    }

    public void getAllUsers(final UserCompletion userCompletion) {

        Flowable<RealmResults<User>> customers = realm.where(User.class).findAllAsync().asFlowable().observeOn(AndroidSchedulers.mainThread());

        DisposableSubscriber disposableSubscriber = new DisposableSubscriber<RealmResults<User>>() {

            @Override
            public void onNext(RealmResults<User> customers) {

                userCompletion.onGetUsers(customers);

            }

            @Override
            public void onError(Throwable t) {

                userCompletion.onGetUsers(new ArrayList<User>());

            }

            @Override

            public void onComplete() {
                Log.e("@@","completed" );
            }

        };

        customers.subscribeWith(disposableSubscriber);

        compositeDisposable.add(disposableSubscriber);


    }

    public void getSearchResultsCustomers(final UserCompletion userCompletion, String value) {

        Flowable<RealmResults<User>> customers = realm.where(User.class).contains("name", value, Case.INSENSITIVE).

                findAllAsync().asFlowable().observeOn(AndroidSchedulers.mainThread());

        DisposableSubscriber disposableSubscriber = new DisposableSubscriber<RealmResults<User>>() {

            @Override
            public void onNext(RealmResults<User> customers) {

                userCompletion.onGetUsers(customers);

            }

            @Override
            public void onError(Throwable t) {

                userCompletion.onGetUsers(new ArrayList<User>());

            }

            @Override

            public void onComplete() {
                Log.e("@@","completed" );
            }

        };

        customers.subscribeWith(disposableSubscriber);

        compositeDisposable.add(disposableSubscriber);

    }


    public void getUser(final UserCompletion userCompletion, int value) {

        Flowable<RealmResults<User>> customers = realm.where(User.class).equalTo("id", value).

                findAllAsync().asFlowable().observeOn(AndroidSchedulers.mainThread());

        DisposableSubscriber disposableSubscriber = new DisposableSubscriber<RealmResults<User>>() {

            @Override
            public void onNext(RealmResults<User> customers) {

                userCompletion.onGetUsers(customers);

            }

            @Override
            public void onError(Throwable t) {

                userCompletion.onGetUsers(new ArrayList<User>());

            }

            @Override

            public void onComplete() {
                Log.e("@@","completed" );
            }

        };

        customers.subscribeWith(disposableSubscriber);

        compositeDisposable.add(disposableSubscriber);

    }

    public void insertUser( final User customer, final UserCompletion customerCompletion) {

        realm.executeTransactionAsync(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {

                realm.insert(customer);

            }

        }, new Realm.Transaction.OnSuccess() {

            @Override
            public void onSuccess() {

                customerCompletion.onInsertUser(null);

            }

        }, new Realm.Transaction.OnError() {

            @Override
            public void onError(Throwable error) {

                AppError appError;

                if(error.getMessage().contains("Value already exists:")) {

                    appError = new AppError(getClass().toString() + customer.toString(), AppError.valueAlreadyExists);

                }else {

                    appError = new AppError(getClass().toString() + customer.toString(), error.getMessage().hashCode());

                }

                customerCompletion.onInsertUser(appError);

            }

        });

    }

}
