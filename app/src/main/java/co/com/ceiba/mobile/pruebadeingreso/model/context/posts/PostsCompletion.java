package co.com.ceiba.mobile.pruebadeingreso.model.context.posts;

import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.model.context.AppError;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.post.Post;

public interface PostsCompletion {

    void onGetUserPosts(List<Post> posts);

    void onInsertPost(AppError appError);

}
