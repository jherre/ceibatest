package co.com.ceiba.mobile.pruebadeingreso.model.context.user.remote;

import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.user.User;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import retrofit2.Call;
import retrofit2.http.GET;

public interface UserRemoteService {

    @GET(Endpoints.GET_USERS)
    Call<List<User>> listUsers();

}
