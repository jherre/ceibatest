package co.com.ceiba.mobile.pruebadeingreso.model.context;

public class AppError {

    public static final int valueAlreadyExists = 0;
    private int errorCode;
    private String domain;

    public AppError(String domain, int errorCode) {

        this.errorCode = errorCode;

        this.domain = domain;

    }

    public int getErrorCode() {

        return errorCode;

    }

    public String getDomain() {

        return domain;

    }

    public void addDomain(String domain) {

        this.domain = domain + " " + domain;

    }

}
