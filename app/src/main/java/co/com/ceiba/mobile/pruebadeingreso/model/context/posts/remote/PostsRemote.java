package co.com.ceiba.mobile.pruebadeingreso.model.context.posts.remote;

import android.util.Log;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.model.context.posts.PostsCompletion;
import co.com.ceiba.mobile.pruebadeingreso.model.domain.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PostsRemote {


    public static void getAllUserPosts(final PostsCompletion userCompletion, int userId) {

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Endpoints.URL_BASE).addConverterFactory(GsonConverterFactory.create()).build();

        PostsRemoteService userRemoteService = retrofit.create(PostsRemoteService.class);

        Call<List<Post>> listUsers = userRemoteService.listPost(userId);

        listUsers.enqueue(new Callback<List<Post>>() {

            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                if(response.isSuccessful()) {

                    List<Post> users = response.body();

                    userCompletion.onGetUserPosts(users);

                    for(Post user : users) {

                        Log.e("user", user.toString());

                    }

                }else {

                    userCompletion.onGetUserPosts(null);

                }

            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

                userCompletion.onGetUserPosts(null);

            }

        });


    }

}
